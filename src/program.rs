//! Pixel shader programs.

use glium;

/// Creates a program from a pixel shader.
///
/// Can be used both for further processing and displaying the output.
/// The output of the shader is only converted to sRGB, when an sRGB
/// target texture or a Frame (default framebuffer) is used for display.
/// During computation using shaders, when RGB target textures are used,
/// the output will be preserved (in linear RGB).
// By default glium enables GL_FRAMEBUFFER_SRGB:
// * https://www.opengl.org/wiki/Framebuffer#Colorspace
// It is not necessary to change this by setting outputs_srgb to false
// when creating the Program:
// when the target texture is in RGB, not in sRGB, no sRGB conversion
// will take place regardless of the value of outputs_srgb (since it is
// not necessary in case of an RGB target).
// But sRGB textures should be avoided during computations, since it would
// result in unnecessary conversions to/from sRGB.
pub fn create<F>(facade: &F, pixel_shader_src: &str)
    -> Result<glium::Program, glium::ProgramCreationError>
    where F: glium::backend::Facade {
    let vertex_shader_src = include_str!("shaders/pixel.vert");
    glium::Program::from_source(facade, vertex_shader_src, pixel_shader_src, None)
}

/// Converts grayscale (texture with F32 format) to color texture.
pub static GRAYSCALE_DISPLAY: &'static str = include_str!("shaders/display_float.frag");

pub static DISPLAY_FLOW: &'static str = include_str!("shaders/display_flow.frag");

/// This program preserves the original colours. It is useful for displaying colour textures.
pub static IDENTITY: &'static str = include_str!("shaders/identity.frag");

/// Creates a program for 2D image processing from a grayscale pixel shader
/// that derives luminance from R, G, B coordinates using the equation
/// Y = 0.299 * R + 0.587 * G + 0.114 * B
pub static TO_GRAYSCALE: &'static str = include_str!("shaders/grayscale.frag");

/// Creates a program with a 5 x 5 gaussian blur filter.
pub static GAUSSIAN_5X5: &'static str = include_str!("shaders/gaussian_5x5.frag");

/// Creates a program with a 3 x 3 gaussian blur filter.
pub static GAUSSIAN_3X3: &'static str = include_str!("shaders/gaussian_3x3.frag");

pub static MULTIPLY: &'static str = include_str!("shaders/mult.frag");

pub static SUBTRACT: &'static str = include_str!("shaders/sub.frag");

pub static DX: &'static str = include_str!("shaders/dx.frag");

pub static DY: &'static str = include_str!("shaders/dx.frag");

pub static FLOW: &'static str = include_str!("shaders/flow.frag");