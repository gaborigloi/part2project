use glium;

use process;
use texture;

use std::mem::drop;

/*/// Computes optical flow.
pub struct Flow<'a, F: 'a> where F: glium::backend::Facade {
    pub dx: &'a process::Process<'a, F>,
    pub dy: &'a process::Process<'a, F>,
    pub multiply: &'a process::Process<'a, F>,
    pub subtract: &'a process::Process<'a, F>,
    pub gaussian_3x3: &'a process::Process<'a, F>,
    pub gaussian_5x5: &'a process::Process<'a, F>,
    pub flow: &'a process::Process<'a, F>
}

impl<'a, F: 'a> Flow<'a, F> where F: glium::backend::Facade {
    /// The input textures remain untouched.
    pub fn calculate(&self, image1: &glium::Texture2d, image2: &glium::Texture2d, output: &glium::Texture2d) {
        assert_eq!(image1.dimensions(), image2.dimensions());
        let (width, height) = image1.dimensions();
        let facade = self.dx.get_facade();
        let create_float_texture = || texture::create_float_texture(facade, width, height).unwrap();
        let image1_g = create_float_texture();
        let image2_g = create_float_texture();
        self.gaussian_3x3.draw(image1, &image1_g);
        self.gaussian_3x3.draw(image2, &image2_g);
        let i_x = create_float_texture();
        let i_y = create_float_texture();
        let i_t = create_float_texture();
        self.dx.draw(&image1_g, &i_x);
        self.dy.draw(&image2_g, &i_y);
        self.subtract.draw_from(&[&image1_g, &image2_g], &i_t);
        let i_xi_y = image1_g; // can reuse old, unused textures this way to avoid creating more temporaries unnecessarily
        let i_xi_x = create_float_texture();
        let i_yi_y = create_float_texture();
        let i_xi_t = create_float_texture();
        let i_yi_t = create_float_texture();
        self.multiply.draw_from(&[&i_x, &i_y], &i_xi_y);
        self.multiply.draw_from(&[&i_x, &i_x], &i_xi_x);
        self.multiply.draw_from(&[&i_y, &i_y], &i_yi_y);
        self.multiply.draw_from(&[&i_x, &i_t], &i_xi_t);
        self.multiply.draw_from(&[&i_y, &i_t], &i_yi_t);
        drop(i_x); // can free textures when there are more then needed
        let m11 = create_float_texture();
        let m21 = create_float_texture();
        let m22 = create_float_texture();
        let b1 = create_float_texture();
        let b2 = create_float_texture();
        self.gaussian_5x5.draw(&i_xi_x, &m11);
        self.gaussian_5x5.draw(&i_xi_y, &m21);
        self.gaussian_5x5.draw(&i_yi_y, &m22);
        self.gaussian_5x5.draw(&i_xi_t, &b1);
        self.gaussian_5x5.draw(&i_yi_t, &b2);
        self.flow.draw_from(&[&m11, &m21, &m22, &b1, &b2], output)
    }
}*/