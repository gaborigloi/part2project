extern crate piston_window;

use self::piston_window::*;

pub fn display_flow(flow: Vec<Vec<(f32, f32)>>) {
    let height = flow.len() as u32;
    let width = flow[0].len() as u32;
    let grid_size = 10;
    let grid_width = width / grid_size;
    let grid_height = height / grid_size;

    let mut window: PistonWindow =
        WindowSettings::new("Hello Piston!", [width, height])
        .exit_on_esc(true).build().unwrap();
    let flow_line = Line::new([1.0, 0.0, 0.0, 1.0], 0.5);
    while let Some(e) = window.next() {
        //TODO example code does not work at https://github.com/PistonDevelopers/piston, see https://github.com/PistonDevelopers/piston_window/pull/137/commits/de16c929b3b58a2a48de0b7b31720cb10a4db4e9
        // - should be updated to latest api
        window.draw_2d(&e, |c, g| {
            // move origin to bottom-left corner, with y axis pointing up
            let transform = c.transform.flip_v().trans(0.0, -(height as f64));
            clear([1.0; 4], g);
            for i in 0..grid_height {
                for j in 0..grid_width {
                    let y = i as f64 * grid_size as f64;
                    let x = j as f64 * grid_size as f64;
                    let v = flow[(i*grid_size) as usize][(j*grid_size) as usize];
                    let vx = v.0 as f64;
                    let vy = v.1 as f64;
                    flow_line.draw_arrow([x, y, x + vx, y + vy],
                        4.0,
                        &DrawState::default(),
                        transform,
                        g);
                }
            }
        });
    }
}