// To publicly re-export public interfaces from private modules: https://github.com/rust-lang/rust/issues/18241
// Includes comment with workaround for stable.
// Fix already in nightly and beta.

use glium;

use std::marker::PhantomData;

/// Marker interface to restrict Texture formats to those defined here.
///
/// Also provides some additional type information about the format.
// Could also convey additional information with other marker traits:
// impl FloatFormat for F32
// Could also use fallbacks, for example F32F32F32 can always be used
// instead of F32F32, since no data will be lost. These could also be used to write to larger textures.
pub trait TextureFormat {
    /// This associated type represents the texture type on the CPU.
    ///
    /// This provides information about the appropriate format,
    /// for example this makes downloading the texture easier.
    type P: glium::texture::PixelValue;
}

pub struct F32;
pub struct F32F32;
pub struct F32F32F32;
pub struct AnySrgbFormat;

// No need for these additional structs, could directly use the CPU representation instead:
// Could just alternatively use f32 instead of F32, (f32, f32) instead of F32F32, ...
impl TextureFormat for F32 {
    type P = f32;
}
impl TextureFormat for F32F32 {
    type P = (f32, f32);
}
impl TextureFormat for F32F32F32 {
    type P = (f32, f32, f32);
}
// Should not be any format, since the shaders ignore the alpha value?
impl TextureFormat for AnySrgbFormat {
    type P = (u8, u8, u8);
}

/// This marker trait represents a colour texture with three components.
pub trait ColourFormat: TextureFormat {}

impl ColourFormat for F32F32F32 {}
impl ColourFormat for AnySrgbFormat {}

/// Texture wrapper containing type information about the texture.
///
/// T: texture type (such as SrgbTexture or Texture2d (RBG)),
/// F: the texture format (for example F32 or F32F32)
// Should express somehow that sRGB textures cannot have floating point format.
pub struct Texture<T, F> where F: TextureFormat {
    texture: T,
    format: PhantomData<F>
}

impl<T, F> Texture<T, F> where F: TextureFormat {
    fn new(texture: T) -> Self {
        Texture {
            texture: texture,
            format: PhantomData
        }
    }
}


/// Conversion to sampler uniform for input to shaders.
///
/// Implementations should be fast and avoid allocating resources,
/// because this is invoked for each draw call.
//trait CanSample<'n>: 'n + Sized where glium::uniforms::Sampler<'n, Self>: glium::uniforms::AsUniformValue {
// Could simply add another trait-implemented function here that returns the sampler Uniform.
pub trait CanSample<'n, T=Self>: Sized {

    fn get_sampler(&'n self) -> glium::uniforms::Sampler<'n, T>;

    /// Returns a sampler with nearest filtering and clamping, suitable for displaying
    /// the texture as a 2D image using a quad and image processing (for example, convolution).
    fn get_pixel_sampler(&'n self) -> glium::uniforms::Sampler<'n, T> {
        self.get_sampler()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    }
}

impl<'n> CanSample<'n> for glium::texture::SrgbTexture2d {
    fn get_sampler(&'n self) -> glium::uniforms::Sampler<'n, Self> {
        self.sampled()
    }
}

impl<'n> CanSample<'n> for glium::texture::Texture2d {
    fn get_sampler(&'n self) -> glium::uniforms::Sampler<'n, Self> {
        self.sampled()
    }
}

impl<'n, T, F> CanSample<'n, T> for Texture<T, F>
    where T: CanSample<'n, T>, F: TextureFormat {
    fn get_sampler(&'n self) -> glium::uniforms::Sampler<'n, T> {
        self.texture.get_sampler()
    }
}


/// We can draw to things implementing this trait, and obtain their dimensions.
///
/// Implementations should be fast and avoid allocating resources,
/// because this is invoked for each draw call.
pub trait AsSurface {
    type S: glium::Surface + Sized;
    // should take mut self since we obtain the surface to draw on it - to *modify* the contents of the texture
    fn as_surface<F>(self, facade: &F) -> Self::S where F: glium::backend::Facade;
    fn get_width(&self) -> u32;
    fn get_height(&self) -> u32;
}

impl<'a> AsSurface for &'a glium::texture::Texture2d {
    type S = glium::framebuffer::SimpleFrameBuffer<'a>;
    fn as_surface<F>(self, _: &F) -> glium::framebuffer::SimpleFrameBuffer<'a> {
        self.as_surface()
    }
    fn get_width(&self) -> u32 { self.width() }
    fn get_height(&self) -> u32 { self.height() }
}

impl<'a> AsSurface for &'a glium::texture::SrgbTexture2d {
    type S = glium::framebuffer::SimpleFrameBuffer<'a>;
    fn as_surface<F>(self, facade: &F) -> glium::framebuffer::SimpleFrameBuffer<'a>
        where F: glium::backend::Facade {
        // Texture2d::as_surface, which reuses the FBO of the texture if it exists, does the same thing
        // - it also creates a new SimpleFramebuffer for each call.
        glium::framebuffer::SimpleFrameBuffer::new(facade, self).unwrap()
    }
    fn get_width(&self) -> u32 { self.width() }
    fn get_height(&self) -> u32 { self.height() }
}

impl<'a, T, F> AsSurface for Texture<T, F>
    where T: AsSurface, F: TextureFormat {
    type S = T::S;
    fn as_surface<Facade>(self, facade: &Facade) -> Self::S
        where Facade: glium::backend::Facade {
        self.texture.as_surface(facade)
    }
    fn get_width(&self) -> u32 { self.texture.get_width() }
    fn get_height(&self) -> u32 { self.texture.get_height() }
}

impl<'a> AsSurface for glium::framebuffer::SimpleFrameBuffer<'a> {
    type S = Self;
    fn as_surface<F>(self, _: &F) -> Self::S { self }
    fn get_width(&'a self) -> u32 { self.get_dimensions().0 }
    fn get_height(&'a self) -> u32 { self.get_dimensions().1 }
}

// conflicting implementations: https://users.rust-lang.org/t/solved-conflicting-implementations-of-custom-trait-on-std-types/5114/4
/*impl<T: glium::Surface> AsSurface for T {
    type S = Self;
    fn as_surface<F>(self, _: &F) -> Self::S { self }
    fn get_width(&self) -> u32 { self.get_dimensions().0 }
    fn get_height(&self) -> u32 { self.get_dimensions().1 }
}*/


use std::error;
pub trait CanCreate: Sized {
    type Error: error::Error;

    fn create<F>(facade: &F, width: u32, height: u32) -> Result<Self, Self::Error>
        where F: glium::backend::Facade;
}

impl CanCreate for Texture<glium::Texture2d, F32> {
    type Error = glium::texture::TextureCreationError;

    fn create<F>(facade: &F, width: u32, height: u32) -> Result<Self, Self::Error>
        where F: glium::backend::Facade {
        let texture = try!(
            glium::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height));
        Ok(Texture::new(texture))
    }
}

impl CanCreate for Texture<glium::Texture2d, F32F32> {
    type Error = glium::texture::TextureCreationError;

    fn create<F>(facade: &F, width: u32, height: u32) -> Result<Self, Self::Error>
        where F: glium::backend::Facade {
        let texture = try!(
            glium::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32F32,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height));
        Ok(Texture::new(texture))
    }
}

impl CanCreate for Texture<glium::Texture2d, F32F32F32> {
    type Error = glium::texture::TextureCreationError;

    fn create<F>(facade: &F, width: u32, height: u32) -> Result<Self, Self::Error>
        where F: glium::backend::Facade {
        let texture = try!(
            glium::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32F32,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height));
        Ok(Texture::new(texture))
    }
}


extern crate image;
use self::image::{ DynamicImage, GenericImage };

/// Upload the texture to the GPU from an image.
fn to_glium_texture<F>(facade: &F, image: &DynamicImage) -> glium::texture::SrgbTexture2d
    where F: glium::backend::Facade {
    let image = image.to_rgba();
    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    // data in the image file is almost always in sRGB, so we have to use an SrgbTexture2d
    glium::texture::SrgbTexture2d::with_mipmaps(facade, image, glium::texture::MipmapsOption::NoMipmap).unwrap()
}

/// Upload the texture to the GPU from an image.
pub fn to_texture<F>(facade: &F, image: &DynamicImage) -> Texture<glium::texture::SrgbTexture2d, AnySrgbFormat>
    where F: glium::backend::Facade {
    Texture::new(to_glium_texture(facade, image))
}

/// Download the texture from the GPU.
pub trait Read<T> {
    fn read(&self) -> T;
}

use std::ops::Deref;

/// Download the texture data from the GPU.
///
/// Reads a texture with type and pixel format T, F into a container D
/// as elements with the equivalent CPU pixel format F::P:
/// T: the underlying texture type in the Texture wrapper, such as SrgbTexture2d or Texture2d,
/// F: the pixel format of the texture, with corresponding CPU pixel format F::P,
/// D: the storage for the data, it will store elements of type F::P
impl<T, F, D> Read<D> for Texture<T, F>
    where F: TextureFormat, D: glium::texture::Texture2dDataSink<F::P>, T: Deref<Target=glium::texture::TextureAny> {
    fn read(&self) -> D {
        read_texture(&self.texture)
    }
}

fn read_texture<T, P>(texture: &glium::texture::TextureAny) -> T
    where T: glium::texture::Texture2dDataSink<P>, P: glium::texture::PixelValue {
    // Could also use raw_read_to_pixel_buffer which can be more efficient:
    // it is done asynchronously so doesn't need a synchronization, unlike raw_read.
    // But the GPU needs to do work during download to exploit this,
    // such as switching between two Pixel Buffer Objects at each computation pass:
    // * https://www.opengl.org/wiki/Pixel_Buffer_Object#Downloads
    texture.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: texture.get_width(), height: texture.get_height().unwrap()
        })
}

pub trait PrintData {
    /// Reads and prints the texture data.
    fn print_texture(name: &str, texture: &Self);
}

use std::fmt::Debug;

impl<T, F> PrintData for Texture<T, F>
    where F: TextureFormat, F::P: Debug, T: Deref<Target=glium::texture::TextureAny> {
    /// Reads and prints the texture both as its own format and (u8, u8, u8, u8).
    fn print_texture(name: &str, texture: &Self) {
        let data: Vec<Vec<F::P>> = texture.read();
        println!("{}: {:?}", name, data);
        let data: Vec<Vec<(u8, u8, u8, u8)>> = read_texture(&texture.texture);
        println!("{}: {:?}", name, data);
    }
}