//! This is my first Rust program ever, so take everything with a pinch of salt!:)

mod program;
pub mod process;
pub mod quad;
mod texture;
pub mod textureio;
pub mod oflow;
pub mod display;

mod uniforms;

pub use texture::*;

#[macro_use]
extern crate glium;

#[macro_use]
#[cfg(test)]
extern crate nalgebra;

/// Creates a headless context if it has a supported GLSL version, otherwise creates a hidden window.
pub fn create_headless_context() -> glium::Display {
    use glium::DisplayBuild;
    let headless_context = glium::glutin::HeadlessRendererBuilder::new(1, 1).build_glium();
    if headless_context.is_ok() {
        let c = headless_context.unwrap();
        if supported(&c) { return c; }
    }
    glium::glutin::WindowBuilder::new().with_visibility(false).with_dimensions(1, 1).build_glium().unwrap()
}

pub fn supported(context: &glium::backend::Context) -> bool {
    context.is_glsl_version_supported(&glium::Version(glium::Api::Gl, 1, 40))
}
