//! Module for image processing with pixel shaders.

use glium;

use texture::*;

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
    tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, tex_coords);

/// Contains the values necessary to draw something on a quad.
pub struct Quad<'a, F: 'a> {
    facade: &'a F,
    draw_parameters: glium::DrawParameters<'a>,
    vertex_buffer: glium::VertexBuffer<Vertex>
}

impl<'a, F> Quad<'a, F> where F: glium::backend::Facade {
    pub fn new(facade: &F) -> Quad<F> {

        let shape = {
            let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
            let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
            let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
            let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

            vec![vertex1, vertex2, vertex3, vertex4]
        };

        Quad {
            facade: facade,
            draw_parameters: pixel_shader_drawparameters(),
            vertex_buffer: glium::VertexBuffer::new(facade, &shape).unwrap()
        }
    }

    pub fn get_facade(&self) -> &F { self.facade }

    pub fn draw_to_surface<S, U>(&self, surface: &mut S, program: &glium::Program, uniforms: &U)
        where S: glium::Surface, U: glium::uniforms::Uniforms {
        surface.draw(&self.vertex_buffer,
                     &glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip),
                     &program,
                     uniforms,
                     &self.draw_parameters).unwrap();
    }
}

fn pixel_shader_drawparameters<'a>() -> glium::DrawParameters<'a> {
    glium::DrawParameters {
        // target surface should have no depth buffer
        // target surface should have no stencil buffer
        multisampling: false,
        dithering: false,
        // no blending, smoothing by default
        .. Default::default()
    }
}