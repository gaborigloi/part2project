// https://github.com/tomaka/glium/issues/1342

extern crate glium;

/// Allows one to merge Uniforms.
///
/// Doesn't allocate (box() the Uniforms on the heap.
pub struct CombinedUniforms<S, T>(S, T) where S: glium::uniforms::Uniforms, T: glium::uniforms::Uniforms;

// could move to Uniforms.add(Uniforms) -> CombinedUniforms or UniformsArray.add(Uniforms) and CombinedUniforms.add(Uniforms)
impl<S, T> CombinedUniforms<S, T> where S: glium::uniforms::Uniforms, T: glium::uniforms::Uniforms {
    pub fn new(a: S, b: T) -> CombinedUniforms<S, T> {
        CombinedUniforms(a, b)
    }
}

impl<S, T> glium::uniforms::Uniforms for CombinedUniforms<S, T>
    where S: glium::uniforms::Uniforms, T: glium::uniforms::Uniforms {
    fn visit_values<'a, F: FnMut(&str, glium::uniforms::UniformValue<'a>)>(&'a self, mut output: F) {
        self.0.visit_values(&mut output);
        self.1.visit_values(&mut output);
    }
}

pub struct UniformsArray<'a, U: 'a, S: 'a> where U: glium::uniforms::AsUniformValue, S: AsRef<str> {
    uniforms: &'a[(S, U)]
}

impl<'a, U, S: AsRef<str>> UniformsArray<'a, U, S> where U: glium::uniforms::AsUniformValue + 'a {
    pub fn new(uniforms: &'a[(S, U)]) -> UniformsArray<'a, U, S> {
        UniformsArray { uniforms: uniforms }
    }
}

impl<'n, U, S: AsRef<str>> glium::uniforms::Uniforms for UniformsArray<'n, U, S> where U: glium::uniforms::AsUniformValue {
    fn visit_values<'a, F: FnMut(&str, glium::uniforms::UniformValue<'a>)>(&'a self, mut output: F) {
        for u in self.uniforms {
            output(u.0.as_ref(), glium::uniforms::AsUniformValue::as_uniform_value(&u.1));
        }
    }
}