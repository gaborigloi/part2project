//! Downloading and uploading texture data from/to the GPU.

extern crate image;

use glium;
use glium::texture::srgb_texture2d::SrgbTexture2d;

use self::image::{ DynamicImage, GenericImage };

pub fn to_texture<F>(facade: &F, image: &DynamicImage) -> SrgbTexture2d
    where F: glium::backend::Facade {
    let image = image.to_rgba();
    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    // data in the image file is almost always in sRGB, so we have to use an SrgbTexture2d
    SrgbTexture2d::with_mipmaps(facade, image, glium::texture::MipmapsOption::NoMipmap).unwrap()
}

pub fn read_texture<P, T>(texture: &glium::texture::TextureAny) -> T
    where T: glium::texture::Texture2dDataSink<P>, P: glium::texture::PixelValue {
    // Could also use raw_read_to_pixel_buffer which can be more efficient:
    // it is done asynchronously so doesn't need a synchronization, unlike raw_read.
    // But the GPU needs to do work during download to exploit this,
    // such as switching between two Pixel Buffer Objects at each computation pass:
    // * https://www.opengl.org/wiki/Pixel_Buffer_Object#Downloads
    texture.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: texture.get_width(), height: texture.get_height().unwrap()
        })
}

/// Reads and prints the texture both as f32 and (u8, u8, u8, u8).
pub fn print_texture(name: &str, texture: &glium::texture::TextureAny) {
    let data: Vec<Vec<f32>> = read_texture(&texture);
    println!("{}: {:?}", name, data);
    let data: Vec<Vec<(u8, u8, u8, u8)>> = read_texture(&texture);
    println!("{}: {:?}", name, data);
}