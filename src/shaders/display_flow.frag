#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex1;

void main() {
    color = vec4(texture(tex1, v_tex_coords).rg / 100.0, 0.0, 1.0);
}