#version 140

in vec2 v_tex_coords;
out float gray;

uniform sampler2D tex1;

void main() {
    // derive luminance from R, G, B co-ordinates
    gray = dot(texture(tex1, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
}