/// input: 6 grayscale (F32) textures - matrix M and vector b, in column-major order
/// output: (u1, u2) velocity components (F32F32 texture format)

#version 140

in vec2 v_tex_coords;
out vec2 result;

uniform sampler2D tex1; // M11
uniform sampler2D tex2; // M21 M12
uniform sampler2D tex3; // M22

uniform sampler2D tex4; // b1
uniform sampler2D tex5; // b2

void main() {
    float M11 = texture(tex1, v_tex_coords).r;
    float M21 = texture(tex2, v_tex_coords).r; // = M12
    float M22 = texture(tex3, v_tex_coords).r;
    // check if the determinant is close to 0
    float singular = step(1.0e-10, M11 * M22 - M21 * M21);
    // arguments in column-major order
    mat2 M = mat2(M11, M21,   // first column
                  M21, M22);  // second column
    vec2 b = vec2(texture(tex4, v_tex_coords).r, texture(tex5, v_tex_coords).r);
    // Could return the normal of the velocity instead of 0 when M is singular.
    result = singular * inverse(M) * b;
}