/// input: two grayscale (F32) textures

#version 140

in vec2 v_tex_coords;
out float result;

uniform sampler2D tex1;
uniform sampler2D tex2;

void main() {
    result = texture(tex1, v_tex_coords).r * texture(tex2, v_tex_coords).r;
}