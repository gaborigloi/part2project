#version 140

in vec2 v_tex_coords;
out float dy;

uniform sampler2D tex1;
uniform float step_x;
uniform float step_y;

void main() {
    dy = texture(tex1, v_tex_coords).r - texture(tex1, v_tex_coords - vec2(0.0, step_y)).r;
}