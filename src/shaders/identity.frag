#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex1;

void main() {
    color = texture(tex1, v_tex_coords);
}