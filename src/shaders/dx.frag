/// input: a grayscale texture

#version 140

in vec2 v_tex_coords;
out float dx;

uniform sampler2D tex1;
uniform float step_x;
uniform float step_y;

void main() {
    // The shader will always sample a 4-component vector,
    // when not all the components stored, the remaining RGB ones will be 0
    // and A will be 1:
    // https://www.opengl.org/wiki/Image_Format#Color_formats
    dx = texture(tex1, v_tex_coords).r - texture(tex1, v_tex_coords - vec2(step_x, 0.0)).r;
}