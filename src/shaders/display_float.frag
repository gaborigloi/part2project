/// Convertes grayscale to color texture.

#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex1;

void main() {
    float v = texture(tex1, v_tex_coords).r;
    color = vec4(v, v, v, 1.0);
}