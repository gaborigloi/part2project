/// input: a grayscale texture
///
/// numbers generated using octave: fspecial("gaussian", 3, 0.5)

#version 140

in vec2 v_tex_coords;
out float blurred;

uniform sampler2D tex1;
uniform float step_x;
uniform float step_y;

void main() {
    vec2 sx = vec2(step_x, 0.0);
    vec2 sy = vec2(step_y, 0.0);
    vec2 c = v_tex_coords;
    blurred =
        0.011344 * texture(tex1, c - sx + sy).r + 0.083820 * texture(tex1, c + sy).r + 0.011344 * texture(tex1, c + sx + sy).r +
        0.083820 * texture(tex1, c - sx).r      + 0.619347 * texture(tex1, c).r      + 0.083820 * texture(tex1, c + sx).r      +
        0.011344 * texture(tex1, c - sx - sy).r + 0.083820 * texture(tex1, c - sy).r + 0.011344 * texture(tex1, c + sx - sy).r;
}