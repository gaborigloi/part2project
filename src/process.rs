// decouple: move program into process as private module

//! Texture processing functions that use pixel shaders.

use glium;
use program;
use uniforms;
use quad::Quad;
use texture::*;

/// Manages drawing from an input to an output on a quad,
/// using a given pixel shader program.
///
/// The input can be anything that implements the CanSample trait.
/// The output should implement As<S: Surface>.
// Could fix the input and output texture types (and the number of inputs)
// in PhantomData to ensure no unnecessary RGB<->sRGB conversions will
// happen during computation, and shaders operate on textures with the expected formats.
pub struct ProgramContext<'a, F: 'a> where F: glium::backend::Facade {
    facade: &'a F,
    quad: Quad<'a, F>,
    dx: glium::Program,
    dy: glium::Program,
    multiply: glium::Program,
    subtract: glium::Program,
    identity: glium::Program,
    gaussian_3x3: glium::Program,
    gaussian_5x5: glium::Program,
    to_grayscale: glium::Program
}

impl<'a, F> ProgramContext<'a, F>
    where F: glium::backend::Facade {
    fn new(facade: &'a F) -> Result<ProgramContext<'a, F>, glium::ProgramCreationError> {
        Ok(ProgramContext {
            facade: facade,
            quad: Quad::new(facade),
            dx: try!(program::create(facade, include_str!("shaders/dx.frag"))),
            dy: try!(program::create(facade, include_str!("shaders/dy.frag"))),
            multiply: try!(program::create(facade, include_str!("shaders/mult.frag"))),
            subtract: try!(program::create(facade, include_str!("shaders/sub.frag"))),
            identity: try!(program::create(facade, include_str!("shaders/identity.frag"))),
            gaussian_3x3: try!(program::create(facade, include_str!("shaders/gaussian_3x3.frag"))),
            gaussian_5x5: try!(program::create(facade, include_str!("shaders/gaussian_5x5.frag"))),
            to_grayscale: try!(program::create(facade, include_str!("shaders/grayscale.frag")))
        })
    }

    // We accept the output as an argument to avoid allocating resources in these calls.
    // Should the corresponding color mask components be disabled when some components
    // are missing from the texture (for example in case of a texture with F32 format)?
    pub fn draw_texture<'b, Input, Surface, Output>(&'a self, texture: &'b Input, program: &glium::Program, output: Output)
        where Input: CanSample<'b>,
            glium::uniforms::Sampler<'b, Input>: glium::uniforms::AsUniformValue,
            Output: AsSurface {
        self.draw_textures(&[texture], program, output);
    }


    // Should make lifetimes as general and flexible as possible.
    pub fn draw_textures<'b, Input, Surface, Output>(&'a self, textures: &[&'b Input], program: &glium::Program, output: Output)
        where Input: CanSample<'b>,
        //TODO also make this ˇ in CanSample an associated type, commit, go back to previous simpler system (but with the new improvements), take a scratch: [&FrameBuffer; 4] array in functions for temporaries and reuse them
              glium::uniforms::Sampler<'b, Input>: glium::uniforms::AsUniformValue,
              Output: AsSurface {
        let mut target = output.as_surface(self.facade);
        let texture_uniforms: Vec<_> =
            textures.iter().enumerate().map(|(i, t)| (format!("tex{}", i + 1), t.get_pixel_sampler())).collect();
        let uniforms = uniforms::CombinedUniforms::new(uniform! {
                step_x: 1.0 / target.get_width() as f32,
                step_y: 1.0 / target.get_height() as f32
            },
            uniforms::UniformsArray::new(&texture_uniforms));
        self.quad.draw_to_surface(&mut target, program, &uniforms);
    }

    // To fix this issue (of having to specify a lifetime for a variable created in the function),
    // - an output cache could be used, so the output would be owned by this struct with a known lifetime
    // - AsSurface could be reverted to using BorrowMut and take references in all the functions.
    //   This way, all the texture interfaces would be implemented for the same types.
    /*/// This draw function creates its output texture.
    pub fn draw_textures_new<'b, Input, Surface, Output>(&'a self, textures: &[&'b Input], program: &glium::Program) -> Output
        // Should be changed: we don't need a Surface here, just the dimensions
        where Input: CanSample<'b> + AsSurface<Surface>,
              glium::uniforms::Sampler<'b, Input>: glium::uniforms::AsUniformValue,
              Output: CanCreate,
              Output: AsSurface<Surface>,
              Surface: glium::Surface + Sized {
        let (width, height) = (textures[0].get_width(), textures[0].get_height());
        let output = Output::create(self.facade, width, height).unwrap();
        self.draw_textures(textures, program, &output);
        output
    }*/
}

/*impl<'a, F> ProgramContext<'a, F>
    where F: glium::backend::Facade {
    pub fn to_grayscale<'a, F, T1, T2, P: ColourFormat>(colour_input: Texture<T1, P>, grayscale_output: Texture<T2, F32>) {
        self.draw_texture(colour_input, surface, output
    }
}*/

/*// Could also just declare processing functions inside a Context object that has
// the shader programs and the necessary resources for drawing, and initializes them
// when it is created.

// To construct Processing objects that statically take a fixed number of arguments,
// and are generic over the number of arguments, could take an A: AsRef<[T]> type parameter.
impl<'a, F> Process<'a, F>
    where F: glium::backend::Facade {
    fn new(quad: &'a Quad<'a, F>, program: glium::Program) -> Process<'a, F> {
        Process {
            quad: quad,
            program: program
        }
    }
    /// Creates a struct suitable for computation.
    ///
    /// The shader will have linear RGB output when the target is an (RGB) Texture2d,
    /// otherwise the output will be converted to sRGB (in case of an SrgbTexture2d or Frame target).
    ///
    /// WARNING: sRGB textures (SrgbTexture2d) should not be used during computation,
    /// using shaders, it would result in unnecessary conversions:
    /// * from sRGB to RGB when the sRGB texture is sampled,
    /// * and from linear RGB to non-linear sRGB when an sRGB texture is used as the target.
    fn from_source(quad: &'a Quad<'a, F>, pixel_shader_src: &str) -> Process<'a, F> {
        let program = program::create(quad.get_facade(), pixel_shader_src).unwrap();
        Self::new(quad, program)
    }
    // We accept the output as an argument to avoid allocating resources in these calls.
    // Should the corresponding color mask components be disabled when some components
    // are missing from the texture (for example in case of a texture with F32 format)?
    pub fn draw<Input, Surface, Output>(&'a self, texture: &'a Input, output: Output)
        where Input: CanSample<'a>,
            glium::uniforms::Sampler<'a, Input>: glium::uniforms::AsUniformValue,
            Output: AsSurface<'a, Surface>,
            Surface: glium::Surface + Sized {
        self.draw_from(&[texture], output);
    }
    // Should make lifetimes as general and flexible as possible.
    pub fn draw_from<Input, Surface, Output>(&'a self, textures: &[&'a Input], output: Output)
        where Input: CanSample<'a>,
            glium::uniforms::Sampler<'a, Input>: glium::uniforms::AsUniformValue,
            Output: AsSurface<'a, Surface>,
            Surface: glium::Surface + Sized {
        let mut target = output.as_surface(self.quad.get_facade());
        let texture_uniforms: Vec<_> =
            textures.iter().enumerate().map(|(i, t)| (format!("tex{}", i + 1), t.get_pixel_sampler())).collect();
        let uniforms = uniforms::CombinedUniforms::new(uniform! {
                step_x: 1.0 / target.get_width() as f32,
                step_y: 1.0 / target.get_height() as f32
            },
            uniforms::UniformsArray::new(&texture_uniforms));
        self.quad.draw(&mut target, &self.program, &uniforms);
    }
    // Could use CanCreate trait for a third convenience method that creates its output: let result = to_grayscale(tex);

    pub fn get_facade(&self) -> &F { self.quad.get_facade() }
}

// Could remove the repetition using impl for ProgramLoader(Quad)?
// (and moving the constants from the program module into functions)

pub fn multiply<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::MULTIPLY)
}

pub fn subtract<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::SUBTRACT)
}

pub fn dx<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::DX)
}

pub fn dy<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::DY)
}

pub fn gaussian_5x5<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::GAUSSIAN_5X5)
}

pub fn gaussian_3x3<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::GAUSSIAN_3X3)
}

pub fn flow<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::FLOW)
}

pub fn display_f32<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::GRAYSCALE_DISPLAY)
}

/// The identity shader. I will return the input colour unchanged.
pub fn display_colour<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::IDENTITY)
}

/// The identity shader. I will return the input colour unchanged.
pub fn display_flow<'a, F>(quad: &'a Quad<F>) -> Process<'a, F>
    where F: glium::backend::Facade {
    Process::from_source(&quad, program::DISPLAY_FLOW)
}*/

#[cfg(test)]
mod tests {
    use super::*;
    use glium;
    use textureio::*;
    use nalgebra::*;

    #[test]
    fn grayscale_is_preserved() {
        let facade = ::create_headless_context();
        let data = vec![vec![(255u8, 255, 255, 255), (128, 128, 128, 255)],
                        vec![(1, 1, 1, 255), (0, 0, 0, 255)]];
        println!("input data: {:?}", data);
        // We upload the data to a Texture2d in linear RGB.
        // Sampling from an sRGB texture will convert the colors to RGB,
        // so the output is the same for both sRGB and RGB textures.
        let input_texture = glium::Texture2d::with_mipmaps(
            &facade,
            data.clone(),
            glium::texture::MipmapsOption::NoMipmap).unwrap();
        print_texture("input_texture", &input_texture);
        let quad = ::quad::Quad::new(&facade);
        let grayscale_texture = ::create_float_texture(&facade, input_texture.width(), input_texture.height()).unwrap();
        let to_grayscale = grayscale_converter(&quad);
        to_grayscale.draw_from(&[&input_texture], &grayscale_texture);
        print_texture("grayscale_texture", &grayscale_texture);
        let output_data: Vec<Vec<f32>> = read_texture(&grayscale_texture);
        for rows in data.iter().zip(output_data) {
            for vals in rows.0.iter().zip(rows.1.iter()) {
                assert_approx_eq!((vals.0).0 as f32 / 255.0, vals.1);
            }
        }
    }

    #[test]
    fn pixel_values_are_preserved() {
        let facade = ::create_headless_context();
        let data = vec![vec![(255u8, 255, 255, 255), (128, 128, 128, 255)],
                        vec![(1, 1, 1, 255), (0, 0, 0, 255)]];
        println!("input data: {:?}", data);
        // We upload the data to a Texture2d in linear RGB.
        // Sampling from an sRGB texture will convert the colors to RGB,
        // so the output is the same for both sRGB and RGB textures.
        let input_texture = glium::Texture2d::with_mipmaps(
            &facade,
            data.clone(),
            glium::texture::MipmapsOption::NoMipmap).unwrap();
        print_texture("input_texture", &input_texture);
        let quad = ::quad::Quad::new(&facade);
        let same_texture = glium::Texture2d::empty(
            &facade,
            input_texture.width(),
            input_texture.height()).unwrap();
        let identity = display_colour(&quad);
        identity.draw(&input_texture, &same_texture);
        print_texture("same_texture", &same_texture);
        let output_data: Vec<Vec<(u8, u8, u8, u8)>> = read_texture(&same_texture);
        for rows in data.iter().zip(output_data) {
            for vals in rows.0.iter().zip(rows.1.iter()) {
                assert_eq!(vals.0, vals.1);
            }
        }
    }
}