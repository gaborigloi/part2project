extern crate flow;
extern crate glium;
extern crate image;

use flow::*;
use flow::textureio::*;

use glium::DisplayBuild;
use std::io::Cursor;
use image::GenericImage;

/// Blocks until escape, space, or enter is pressed or the window close button is clicked.
fn pause(display: &glium::Display) {
    use glium::glutin::Event::KeyboardInput;
    use glium::glutin::{VirtualKeyCode, ElementState};

    'outer: loop {
        for ev in display.wait_events() {
            match ev {
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Space))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Escape))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Return))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::NumpadEnter))
                | glium::glutin::Event::Closed => break 'outer,
                _ => ()
            }
        }
    }
}

#[test]
// ignore visual tests
#[ignore]
fn convert_to_grayscale() {
    let image = image::load(Cursor::new(&include_bytes!("resources/parrot_colour.jpg")[..]),
                            image::JPEG).unwrap();
    let display = glium::glutin::WindowBuilder::new().with_dimensions(image.width(), image.height()).build_glium().unwrap();
    let quad = quad::Quad::new(&display);
    let to_grayscale = flow::process::grayscale_converter(&quad);
    let texture = to_texture(&display, &image);
    let grayscale_texture = create_float_texture(&display, texture.width(), texture.height()).unwrap();
    to_grayscale.draw(&texture, &grayscale_texture);
    {
        let display_colour = flow::process::display_colour(&quad);
        let mut target = display.draw();
        display_colour.draw(&texture, &mut target);
        target.finish().unwrap();
    }
    pause(&display);
    {
        let display_f32 = flow::process::display_f32(&quad);
        let mut target = display.draw();
        display_f32.draw(&grayscale_texture, &mut target);
        target.finish().unwrap();
    }
    pause(&display);
}

#[test]
#[ignore]
fn display_dx() {
    let image = image::load(Cursor::new(&include_bytes!("resources/parrot_colour.jpg")[..]),
                            image::JPEG).unwrap();
    let display = glium::glutin::WindowBuilder::new().with_dimensions(image.width(), image.height()).build_glium().unwrap();
    let quad = quad::Quad::new(&display);
    let to_grayscale = process::grayscale_converter(&quad);
    let dx = process::dx(&quad);
    let texture = to_texture(&display, &image);
    let grayscale_texture = create_float_texture(&display, texture.width(), texture.height()).unwrap();
    let dx_texture = create_float_texture(&display, texture.width(), texture.height()).unwrap();
    to_grayscale.draw(&texture, &grayscale_texture);
    {
        let display_colour = flow::process::display_colour(&quad);
        let mut target = display.draw();
        display_colour.draw(&texture, &mut target);
        target.finish().unwrap();
    }
    dx.draw(&grayscale_texture, &dx_texture);
    pause(&display);
    {
        let display_f32 = flow::process::display_f32(&quad);
        let mut target = display.draw();
        display_f32.draw(&dx_texture, &mut target);
        target.finish().unwrap();
    }
    pause(&display);
}

#[test]
#[ignore]
fn display_flow() {
    println!("loading images...");
    let image1 = image::load(Cursor::new(&include_bytes!("resources/1083.png")[..]),
                            image::PNG).unwrap();
    let image2 = image::load(Cursor::new(&include_bytes!("resources/1084.png")[..]),
                            image::PNG).unwrap();
    println!("images loaded.");
    let width = image1.width();
    let height = image1.height();
    let display = glium::glutin::WindowBuilder::new().with_dimensions(width, height).build_glium().unwrap();
    println!("uploading images to textures...");
    let texture1 = to_texture(&display, &image1);
    let texture2 = to_texture(&display, &image2);
    println!("uploaded.");
    let texture1_grayscale = create_float_texture(&display, width, height).unwrap();
    let texture2_grayscale = create_float_texture(&display, width, height).unwrap();
    let quad = quad::Quad::new(&display);
    let to_grayscale = process::grayscale_converter(&quad);
    let output_texture = glium::Texture2d::empty_with_format(
        &display,
        glium::texture::UncompressedFloatFormat::F32F32,
        glium::texture::MipmapsOption::NoMipmap,
        width,
        height).unwrap();
    let flow = oflow::Flow {
        dx: &process::dx(&quad),
        dy: &process::dy(&quad),
        multiply: &process::multiply(&quad),
        subtract: &process::subtract(&quad),
        gaussian_3x3: &process::gaussian_3x3(&quad),
        gaussian_5x5: &process::gaussian_5x5(&quad),
        flow: &process::flow(&quad)
    };
    let display_f32 = flow::process::display_f32(&quad);
    let display_flow = flow::process::display_flow(&quad);
    flow.calculate(&texture1_grayscale, &texture2_grayscale, &output_texture);
    {
        to_grayscale.draw(&texture1, &texture1_grayscale);
        let mut target = display.draw();
        display_f32.draw(&texture1_grayscale, &mut target);
        target.finish().unwrap();
    }
    pause(&display);
    {
        to_grayscale.draw(&texture2, &texture2_grayscale);
        let mut target = display.draw();
        display_f32.draw(&texture2_grayscale, &mut target);
        target.finish().unwrap();
    }
    pause(&display);
    {
        let mut target = display.draw();
        display_flow.draw(&output_texture, &mut target);
        target.finish().unwrap();
    }
    display::display_flow(read_texture(&output_texture));
    pause(&display);
}

#[test]
fn grayscale_srgb_values_preserved() {
    let facade = create_headless_context();
    let quad = quad::Quad::new(&facade);
    let srgb_texture = {
        let image = image::load(Cursor::new(&include_bytes!("resources/test_values_srgb.png")[..]),
                                image::PNG).unwrap();
        to_texture(&facade, &image)
    };
    print_texture("srgb input (grayscale)", &srgb_texture);
    let grayscale_texture = create_float_texture(&facade, srgb_texture.width(), srgb_texture.height()).unwrap();
    {
        let to_grayscale = flow::process::grayscale_converter(&quad);
        to_grayscale.draw(&srgb_texture, &grayscale_texture);
    }
    print_texture("grayscale RGB output", &grayscale_texture);
    let rbg_expected = vec![
        vec![( 16,     16,  16), (  8,   8,   8), (  1,   1,   1), (  0,   0,   0)],
        vec![(255_u8, 255, 255), (128, 128, 128), ( 64,  64,  64), ( 32,  32,  32)]];
    println!("expected RGB output: {:?}", &rbg_expected);
    let output_data: Vec<Vec<(u8, u8, u8, u8)>> = read_texture(&grayscale_texture);
    for rows in rbg_expected.iter().zip(output_data) {
        for (expected, output) in rows.0.iter().zip(rows.1.iter()) {
            assert_eq!(expected.0, output.0);
        }
    }
}